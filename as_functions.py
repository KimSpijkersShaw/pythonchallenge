#!/usr/bin/env python
''' Functions to add things to the database '''

import sys
import json
from datetime import date
import mysql.connector
import requests
import searchAPI

def get_woeid(city):
    " Gets the location ID of a gievn city from the API "
    url = 'https://www.metaweather.com/api/location/search/?query=' + city.replace(" ", "%20")
    r = requests.get(url)
    q = json.loads(r.content)
    if q == []:
        print(f"City {city} not found on API server.")
        return None
    return q[0]['woeid']

def connect_to_db():
    " Connects to the weather database"
    try:
        dbconn = mysql.connector.connect(user = 'kim', host = '54.72.160.99', database = 'weather', password = 'AL2020')
    except:
        return "Could not connect to database"
        sys.exit(1)
    print("Connected to database server")
    return dbconn

def add_city(woeid):
    " Adds a city to the locations table "
    dbconn = connect_to_db()
    weacur = dbconn.cursor(dictionary=True)
    insertsql = "REPLACE INTO locations (woeid, city, latt_long, timezone, sun_rise, sun_set) VALUES(%s,%s,%s,%s,%s,%s)"
    sqlvalues = searchAPI.get_loc_info(woeid)
    try:
        weacur.execute(insertsql, sqlvalues)
    except mysql.connector.errors.ProgrammingError:
        print("Bad SQL syntax")
    try:
        dbconn.commit()
    except:
        print("Insert failed")
    weacur.close()
    dbconn.close()

def add_weather(woeid, date=date.today().strftime("%Y/%m/%d")):
    " Adds today's 5-day weather report for a given location to the weather_readings table "
    dbconn = connect_to_db()
    weacur = dbconn.cursor(dictionary=True)
    insertsql = "REPLACE INTO weather_readings (woeid, weather_id, date_taken, time_taken, weather_state, wind_dir, min_temp, max_temp, acc_temp, air_pressure, humidity, predictability) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"
        
    # try:
    sqlvalues = searchAPI.get_wthr_info(woeid)
    print(sqlvalues)
    weacur.execute(insertsql, sqlvalues)
    dbconn.commit()
    # except mysql.connector.errors.ProgrammingError:
    #     print("Bad SQL syntax")
    weacur.close()
    dbconn.close()

