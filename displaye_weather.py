#!/usr/bin/env python
''' Interactive command line script to display information from the weather database'''
import as_functions

def print_db_output(lst):
    count = 0
    for key in lst[0].keys():
        print(f"{key.replace('_',' '):<15}", end = '')
        count += 1
    print(" ")
    print("-" * (count * 15))
    for x in lst:
        for key in x.keys():
            print(f"{x[key]:<15}", end = '')
        print(" ")

done = False
while not done:
    choice1 = input("What would you like to do? \n A) List cities \n B) Find city woeid for any city \n C) Get a weather report \n (Type a/b/c)")

    dbconn = as_functions.connect_to_db()
    weacur = dbconn.cursor(dictionary=True, buffered=True)

    if choice1.lower() == 'a':
        choice2 = input("Would you like the woeid's too? (y/n)")
        if choice2 == 'n':
            weacur.execute('SELECT city FROM locations;')
            locs = weacur.fetchall()
            print_db_output(locs)
            done = True
        elif choice2 == 'y':
            weacur.execute('SELECT city, woeid FROM locations;')
            done = True
            locs = weacur.fetchall()
            print_db_output(locs)
        else:
            quit = input("That was not an option sorry. \n Press y to try again or any other key to quit.")
            if quit.lower() == 'y':
                continue
            else: 
                done = True

    elif choice1.lower() == 'b':
        choice2 = input("City name: ")
        woeid = as_functions.get_woeid(choice2)
        if woeid:
            print(f"{choice2} woeid is: {woeid}")
            done = True
        else:
            quit = input("That city is not in the system, sorry. \n Press y to try again or any other key to quit.")
            if quit.lower() == 'y':
                continue
            else: 
                done = True

    elif choice1.lower() == 'c':

        choice2 = input("City name: ")
        choice3 = input("Date (YYYY-MM-DD): ")
        woeid = as_functions.get_woeid(choice2)
        if woeid:
            print(f"Weather report for {choice2} on {choice3}")
            print("-" * 40)
            weacur.execute(f"SELECT AVG(predictability) AS AvPred FROM weather_readings WHERE woeid = {woeid} AND date_taken = '{choice3}';")
            predictability = weacur.fetchall()
            print(f"Average predictability score: {predictability[0]['AvPred']:.2f}%")
            weacur.execute(f"SELECT AVG(min_temp) AS AvMinTemp FROM weather_readings WHERE woeid = {woeid} AND date_taken = '{choice3}';")
            mintemp = weacur.fetchall()
            print(f"Average minimum temperature recorded: {mintemp[0]['AvMinTemp']:.2f}")
            weacur.execute(f"SELECT AVG(max_temp) AS AvMaxTemp FROM weather_readings WHERE woeid = {woeid} AND date_taken = '{choice3}';")
            maxtemp = weacur.fetchall()
            print(f"Average maximum temperature recorded: {maxtemp[0]['AvMaxTemp']:.2f}")
            weacur.execute(f"SELECT time_taken, weather_state FROM weather_readings WHERE woeid = {woeid} AND date_taken = '{choice3}';")
            states = weacur.fetchall()
            print(f"Hourly weather states:")
            print_db_output(states)
            done = True
        else:
            quit = input("Could not find record for that city, please check the city list.\nPress y to try again or any other key to quit.")
            if quit.lower() == 'y':
                continue
            else: 
                done = True

    else:
        quit = input("That option isn't available, Press y to try again or any other key to quit.")
        if quit.lower() == 'y':
            continue
        else: 
            done = True
