#!/usr/bin/env python

import as_functions
import sys
import mysql.connector
import searchAPI
import json
import requests

cityList = ['london', 'edinburgh', 'new york', 'tokyo', 'sydney', 'fesf']

for city in cityList:
    woeid = as_functions.get_woeid(city)
    if woeid:
        as_functions.add_city(woeid)