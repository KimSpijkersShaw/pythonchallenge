#!/bin/env python3
'''  ''' 
import as_functions
import requests


def get_city_name(woeid):
    dbconn = as_functions.connect_to_db()
    weacur = dbconn.cursor(dictionary=True, buffered=True)
    weacur.execute(f"SELECT city FROM weather.locations WHERE woeid = {woeid}")
    return weacur.fetchall()[0]['city']

def slack_report(message):
    url = "https://hooks.slack.com/services/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"
    payload = """
    {
    "channel": "#academy_api_testing",
    "username": "WeatherZone",
    "text": """
    payload += '"' + message +'"'
    payload += """,
    "color": "warning",
    "icon_url": "https://www.clipartkey.com/mpngs/m/312-3128077_weather-zone-weather-logo-for-kids.png"
    }"""
    r = requests.post(url, data=payload)
    print(r.status_code)

def write_message(city, temp, adj, minmax):
    city_name = get_city_name(city['woeid'])
    message = f"It's {adj} in {city_name} today! \n The {minmax}imum temperature is {temp:.2f}."
    slack_report(message)

def generate_report(minmax, adj):
    dbconn = as_functions.connect_to_db()
    weacur = dbconn.cursor(dictionary=True, buffered=True)
    weacur.execute(f"SELECT woeid, {minmax.upper()}({minmax}_temp) AS {minmax}Temp FROM weather.weather_readings WHERE date_taken = CURRENT_DATE() GROUP BY woeid;")
    temps = weacur.fetchall()
    for city in temps:
        temp = city[f'{minmax}Temp']
        if minmax == 'min' and temp < 0:
            write_message(city, temp, adj, minmax)
        elif minmax == 'max' and temp > 30:
            write_message(city, temp, adj, minmax)

generate_report('min', 'cold')
generate_report('max', 'hot')

