''' Gets location and weather info from the API '''

import json
import requests
from datetime import date


#https://www.metaweather.com/api/location/search/?query=london

def get_loc_info(woeid):
    url = 'https://www.metaweather.com/api/location/' + str(woeid) + '/'
    r = requests.get(url)
    q = json.loads(r.content)
    s = json.loads(r.content)['consolidated_weather']
    title = q['title']
    woeid = q['woeid']
    lt_lg = q['latt_long']
    tzone = q['timezone']
    sunrise = q['sun_rise'][11:16]
    sunset = q['sun_set'][11:16]

    return [woeid, title, lt_lg, tzone, sunrise, sunset]

def get_wthr_info(woeid):
    url = 'https://www.metaweather.com/api/location/' + str(woeid) + '/' 
    r = requests.get(url)
    q = json.loads(r.content)
    s = json.loads(r.content)['consolidated_weather']
 #   t=list(map(lambda x: [woeid , x['applicable_date'], x['created'][11:16] , x['weather_state_name'], x['wind_direction_compass'], x['min_temp'], x['max_temp'], x['the_temp'], x['air_pressure'], x['humidity'], x['predictability']],s))[0:1]
    t=[woeid, s[0]['id'] , s[0]['applicable_date'], s[0]['created'][11:16] , s[0]['weather_state_name'], s[0]['wind_direction_compass'], s[0]['min_temp'], s[0]['max_temp'], s[0]['the_temp'], s[0]['air_pressure'], s[0]['humidity'], s[0]['predictability']]

    return t

#t=list(map(lambda x: f"[{x['id']}, {x['applicable_date']}, {x['weather_state_name']}, {x['wind_direction_compass']}, {x['min_temp']}, {x['max_temp']}, {x['the_temp']}, {x['air_pressure']}, {x['humidity']}, {x['predictability']}, {['---']},{['---']}]", s)) [0:5]

#woeid, date, state, wind, min_tem, max_temp, acc_temp, air_pressure, humidity, predictability, sun_rise, sun_set - DATABASE ORDER

#strt = ' '.join(t)

# url = "https://hooks.slack.com/services/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"
# message = f"Countries are: {strt}"
# payload = """
# {
#  "channel": "#academy_api_testing",
#  "username": "Thor",
#  "text": """
# payload += '"' + message +'"'
# payload += """,
#  "color": "warning",
#  "icon_url": "https://www.searchpng.com/wp-content/uploads/2019/02/Avengers-Logo-PNG-Transparent-Avengers-Logo-715x715.png"
# }"""
# r = requests.post(url, data=payload)
# print(r.status_code)

# print(t,w, ltlg, time)
# print(tab1)
# print(len(tab1))
#print(t)

