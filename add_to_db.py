#!/usr/bin/env python

import sys
import mysql.connector
import searchAPI

try:
    dbconn = mysql.connector.connect(user = 'kim', host = 'ec2-34-253-25-188.eu-west-1.compute.amazonaws.com', database = 'weather', password = 'AL2020')
except:
    print("Could not connect to database")
    sys.exit(1)
print("Connected to database server")

weacur = dbconn.cursor(dictionary=True)
#weacur.execute("DESC locations")

insertsql = "REPLACE INTO locations (woeid, city, latt_long, timezone) VALUES(%s,%s,%s,%s)"
#info_list = ['31278', 'Oxford', '329874_98q5985', 'London/Eurpoe']
sqlvalues = searchAPI.get_loc_info(2487956)
try:
    weacur.execute(insertsql, sqlvalues)
except mysql.connector.errors.ProgrammingError:
    print("Bad SQL syntax")
    sys.exit(1)
try:
    dbconn.commit()
except:
    print("Insert failed")
    sys.exit(1)

weacur.close()
dbconn.close()
