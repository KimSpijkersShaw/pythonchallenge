DROP DATABASE IF EXISTS weather;

CREATE DATABASE IF NOT EXISTS weather;

use weather;

CREATE TABLE IF NOT EXISTS locations (
	woeid integer primary key,
	city varchar(100) not null,
    latt_long varchar(15) not null,
--    longitude varchar(15),
    timezone varchar(50),
    sun_rise varchar(20),
    sun_set varchar(20)
);

CREATE TABLE IF NOT EXISTS weather_readings (
	woeid integer ,
	weather_id bigint primary key,
    date_taken varchar(15) not null,
    time_taken varchar(15) not null,
	weather_state varchar(100) not null,
    wind_dir varchar(15),
    min_temp float,
    max_temp float,
    acc_temp float,
    air_pressure float,
    humidity integer,
    predictability integer
  --  PRIMARY KEY (woeid),
    -- FOREIGN KEY (woeid) REFERENCES locations(woeid)
);

-- --- Need if loop so it doesn't error
-- CREATE USER 'kim'@'%' IDENTIFIED BY 'AL2020';
-- GRANT ALL PRIVILEGES ON *.* TO 'kim'@'%';
-- CREATE USER 'hassan'@'%' IDENTIFIED BY 'AL2020';
-- GRANT ALL PRIVILEGES ON *.* TO 'hassan'@'%';
-- CREATE USER 'slackbot'@'%' IDENTIFIED BY 'AL2020';
-- GRANT ALL PRIVILEGES ON *.* TO 'slackbot'@'%';