#!/usr/bin/env python
''' Adds today's weather and the weather in the next five days for cities in list given '''

from datetime import date
import as_functions

import sys
import mysql.connector
import searchAPI
import json
import requests

cityList = ['london', 'edinburgh', 'new york', 'tokyo', 'sydney']

for city in cityList:
    woeid = as_functions.get_woeid(city)
    if woeid:
        as_functions.add_weather(woeid, date.today().strftime("%Y/%m/%d"))