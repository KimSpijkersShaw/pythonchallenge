# README : Python challenge - Kim and Hassan #
This python project contains scripts that will:
- Create and update a database containing weather information from a weather API
- Allow users on the command line to retrieve information they want
- Send a slack update when the temperature in chosen locations is above or below a particular threshold.

The database updates and slack messages have to be automated using a Jenkins server.

## Setup and use ##

### Pre-requisites ###
- A database server running mysql with a public IP address and port 3306 open.
- A Jenkins server you can use for automation of scripts.

### Database building ###
- A databas eserver must be made that can run mysql.
- The script **weather_db.sql** can be run to create the schema required for the rest of the scritps.
- This schme makes a database called weather with two tables
  - locations contains static information about each of the cities (woeid, city, lattitude and longitude, timezone, sun rise time, sun set time)
  - weather_readings contains all the weather information for each city at each measurement time over the course of the day (woeid, weather ID, date taken, time taken, weather state, wind direction, minimum temperature, maximum temperature, actual temperature, air pressure, humidity, predictability)

### Adding information to the database ###
- The script **update_cities.py** can be edited to include desired cities for the database, and run to add their information to it.
  - This script should be run once a day to update sunrise/sunset times or any time a new city is added to the database.
- The script **db_update.py** can be edited to include chosen cities and should be run automatically every 15 minutes to update the weather information for the chosen cities in the database.
  - Cities chosen in this script must have been added to the database using theupdate cities script first for full functionality.
  - The functions used to access the API can be found in **searchAPI&#46;py**.
  - The functions used to add information to the database can be found in **as_functions.py**.

### Command line user interface ###
- Run the file **displaye_weather.py** on a command line in python3 to interactively access information from the database.
- It does not require any initial arguments.
- It will ask for input to choose your output information:
  - City list; shows the cities available on the database.
  - City woeid search; search for the weather id of any city in the world.
  - Weather report by city and date.

### Slack extreme weather notifications ###
- The script **slack_extreme_temp.py** will send an automated message to a slack channel if the mximum/minimum temperature in any location is above/below a given threshold.
- The particular parameters can be set in this file
  - Slack channel webhook can be set using the variable url in slack_report
  - Upper and lower weather limits can be set in the conditions in generate_report on lines 40 and 43 respectively.